/*
	to create a react app,
		- npx create-react-app project-name

	to run our react application
		- npm start

	files to be removed:
	// from src folder:
		App.test.js
		index.css
		reportWebVitals.js
		logo.svg
	// also we need to delete importations of said files (in index.js and app.js)


	The 'import' statement allows us to use the code/export modules from other files similar to how we use the "require" function in NodeJS


	React JS applies the concepts of rendering and mounting in order to display and create components.
	// Rendering refers to the process of calling/invoking a component returning a set of instructions creating DOM
	// Mounting is when React JS 'renders' or 'displays' the component the initial DOM based on the instructions


	Using the event.target.value will capture the value inputted by the user on our input area.
	
	The dependencies in useEffect
	1. Single dependency  [dependency]
		// on the initial load of the component the side effect/function will be triggered and whenever changes occur on our dependency  []
	2. Empty dependency
		// the side effect will only run during the initial load
	3. Multiple dependency  [dependency1, dependency2]
		// the side effect will run during the initial load and whenever the state of the dependencies change


//[SECTION] react-router-dom
	- for us to be able to use the module/library across all of our pages we have to contain them with BrowserRouter/Router

	// Routes - contains all of our routes in our react-app
	// Route - specific route wherein we will declare the url and also the component or page to be mounted

	// We use Link if we want to go from one page to another page
	// We use NavLink if we want to change our page using our NavBar

	//The "localSorate.setItem" allows us to manipulate the brower's localStorage property to store information indefinitely

// [SECTION] Environment Variable
	// environment variables are important in hiding sensitive pieces of information like the backend API which can be exploited if added directly into our code

*/