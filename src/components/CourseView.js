
import {useState, useEffect} from 'react'
import {Container, Row, Col, Card, Button} from 'react-bootstrap'
import {useParams, useNavigate} from 'react-router-dom'
import Swal from 'sweetalert2'


export default function CourseView() {

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
	const navigate = useNavigate()

	// get the parameter from the url using the "useParams"

	const {courseId} = useParams()

	useEffect(() => {
		console.log(courseId)

		fetch(`${process.env.REACT_APP_API_URL}/course/${courseId}`)
		.then(result => result.json())
		.then(data => {
			console.log(data)

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price)
		})

	}, [courseId])

	const enroll = (id) => {
		// we are going to fetch the route of the login 
		fetch(`${process.env.REACT_APP_API_URL}/user/enroll/${id}`, {
			method: 'Post',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(result => result.json())
		.then(data => {
			console.log(data)
			if(data === true){
				Swal.fire({
					title: "Successfuly enrolled!",
					icon: "success",
					text: "Please wait for the final schedule.",
					timer: 5000
				})
				navigate("/courses")
				
			}
			else {
				Swal.fire({
					title: "Admin users are not allowed to enroll!",
					icon: "error",
					showConfirmButton: false,
					timer: 1500
				})
			}
		})
	}

	return(
		<Container className="mt-5">
            <Row>
                <Col lg={{ span: 6, offset: 3 }}>
                    <Card>
                        <Card.Body className="text-center">
                            <Card.Title>{name}</Card.Title>
                            <Card.Subtitle>Description:</Card.Subtitle>
                            <Card.Text>{description}</Card.Text>
                            <Card.Subtitle>Price:</Card.Subtitle>
                            <Card.Text>PhP {price}</Card.Text>
                            <Card.Subtitle>Class Schedule</Card.Subtitle>
                            <Card.Text>8 am - 5 pm</Card.Text>
                            <Button variant="primary" onClick = {() => enroll(courseId)}>Enroll</Button>
                        </Card.Body>        
                    </Card>
                </Col>
            </Row>
		</Container>
	)
}