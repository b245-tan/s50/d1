// import Bootstrap classes
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import UserContext from "../UserContext.js"

/*import {Container, Nav, Navbar} from 'react-bootstrap/Navbar'*/

import { Link, NavLink } from 'react-router-dom'
import { useContext, Fragment } from 'react'

export default function AppNavBar(){
	// if you want to get the item in our localStorage you can use the .getItem("property")

	// let us create a new state for the user
	// const [user, setUser] = useState(localStorage.getItem("email"))

	const {user} = useContext(UserContext);

	return(
		<Navbar bg="light" expand="lg">
		      <Container>
		      {/*Using the 'as' keyword, Navbar.Brand inherits the properties of the Link the keyword Links the Navbar.Brand to the declared url endpoint*/}
		        <Navbar.Brand as = {Link} to = "/">Zuitt</Navbar.Brand>
		        <Navbar.Toggle aria-controls="basic-navbar-nav" />
		        <Navbar.Collapse id="basic-navbar-nav">
		          <Nav className="ms-auto">
		            <Nav.Link as = {NavLink} to = "/">Home</Nav.Link>
		            <Nav.Link as = {NavLink} to = "/courses">Courses</Nav.Link>

		            {/*Conditional Rendering*/}
		            {
		            	user ? 
		            	<Nav.Link as = {NavLink} to = "/logout">Logout</Nav.Link>
		            	:
		            	<Fragment>
		            		<Nav.Link as = {NavLink} to = "/register">Register</Nav.Link>
		            		<Nav.Link as = {NavLink} to = "/login">Login</Nav.Link>
		            	</Fragment>

		            }
		            
		          </Nav>
		        </Navbar.Collapse>
		      </Container>
		    </Navbar>
		)
}