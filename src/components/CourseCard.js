import {Row , Col} from 'react-bootstrap';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import {useState, useEffect, useContext} from 'react'

import UserContext from "../UserContext.js"
import {Link} from 'react-router-dom'

export default function CourseCard({courseProp}){


	// The "course" in the CourseCard component is called a "prop" which is a shorthand for property

	// The curly braces are used for props to signifiy that we are providing information using expressions

	const {_id, name, description, price} = courseProp

	// Use the state hook for this component to be able to store its state
	// States are used to keep track of information related to individual components
		// const [getter, setter] = useState(initialGetterValue);

	const [enrollees, setEnrollees] = useState(0)

// [ACTIVITY]------------------------------------------------
	const [seats, setSeats] = useState(30)

	// add new state that will declare 
	const [isDisabled, setIsDisabled] = useState(false);


	// initial value of enrollees state
	// console.log(enrollees)

	// if you want to change/reassign the value of the state
	// setEnrollees(1)

	const {user} = useContext(UserContext)

	function enroll(){
		
		if(seats <= 0){
			alert('No more seats available')
		}
		else {
			setEnrollees(enrollees+1)
			setSeats(seats-1)
		}
	}
// ----------------------------------------------------------

	// Define a 'useEffect' hook to have the "CourseCard" component do perform a certain task.

	// This will run automatically.
	/*
		Syntax:
			useEffect(sideEffect/function, [dependencies])

			// sideEffect/function will run on the first load and will reload depending on the dependency array
	*/

	useEffect(() => {
		if(seats === 0){
			setIsDisabled(true)
		}
	}, [seats])

	return(
		<Row className = "mt-5">
				{/*First Card*/}
				<Col>
					<Card>
					     <Card.Body>
		                     <Card.Title>{name}</Card.Title>
		                     <Card.Subtitle>Description:</Card.Subtitle>
		                     <Card.Text>{description}</Card.Text>
		                     <Card.Subtitle>Price:</Card.Subtitle>
		                     <Card.Text>{price}</Card.Text>

		                     <Card.Subtitle>Enrollees:</Card.Subtitle>
		                     <Card.Text>{enrollees}</Card.Text>

		                     <Card.Subtitle>Available Seats:</Card.Subtitle>
		                     <Card.Text>{seats}</Card.Text>

		                     {
		                     	user ?
		                     	<Button as = {Link} to = {`/course/${_id}`} variant="primary" disabled = {isDisabled}>See details</Button>
		                     	:
		                     	<Button as = {Link} to = "/login">Login</Button>
		                     }

		                     
		                     </Card.Body>
					   </Card>
				</Col>
		</Row>
		)
}

