import {Button, Form} from 'react-bootstrap';
import {Fragment} from 'react';

// import the hooks that are needed in our page
import {useContext, useState, useEffect} from 'react'
import {Navigate, useNavigate} from "react-router-dom"
import Swal from 'sweetalert2'

import UserContext from '../UserContext.js'

export default function Login(){

	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");

	const navigate = useNavigate();

	// const [user, setUser] = useState(localStorage.getItem("email"))

	// Allows us to consume the UserContext obejct and its properties for user validation

	const {user, setUser} = useContext(UserContext)


	const [isActive, setIsActive] = useState(false)


	useEffect(() => {
		if(email !== "" && password !== ""){
			setIsActive(true)
		}
		else {
			setIsActive(false)
		}
	}, [email, password])




	function login(event){
		event.preventDefault()


		// if you want to add the email of the authenticated user in the local storage
		// Syntax: 
			 //localStorage.setItem("propertName", value)


		// Process a fetch request to corresponding backend API
		// Syntax: fetch('url', {options})

		fetch(`${process.env.REACT_APP_API_URL}/user/login`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(result => result.json())
		.then(data => {
			console.log(data)
			if(data === false) {
				Swal.fire({
					title: "Authentication failed!",
					icon: "error",
					text: "Please try again!",
					showConfirmButton: false,
					timer: 1500
				})
			}
			else{
				localStorage.setItem('token', data.auth)
				retrieveUserDetails(localStorage.getItem("token"))

				Swal.fire({
					title: "Authentication successful!",
					icon: "success",
					text: "Welcome to Zuitt!",
					showConfirmButton: false,
					timer: 2000
				})

				navigate("/")
			}
		})


		/*localStorage.setItem("email", email)
		setUser(localStorage.getItem("email"))

		alert("You are now logged in!")
		setEmail('')
		setPassword('')*/
	}


	const retrieveUserDetails = (token) => {

		// the token sent as part of the request's header information
		fetch(`${process.env.REACT_APP_API_URL}/user/details`, {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(data => {
			console.log(data)

			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	}

	return(

		user ?
		<Navigate to = "/*"/>
		:
		<Fragment>
		<h1 className = "text-center mt-5">Login</h1>
		<Form className = 'mt-5 mx-5' onSubmit = {event=> login(event)}>
		      <Form.Group className="mb-3" controlId="formBasicEmail">
		        <Form.Label>Email address</Form.Label>
		        <Form.Control 
			        type="email" 
			        placeholder="Enter email" 
			        value ={email}
			        onChange = {event => setEmail(event.target.value)}
			        required
			        />
		        <Form.Text className="text-muted">
		        </Form.Text>
		      </Form.Group>

		      <Form.Group className="mb-3" controlId="formBasicPassword">
		        <Form.Label>Password</Form.Label>
		        <Form.Control 
			        type="password" 
			        placeholder="Password" 
			        value ={password}
			        onChange = {event => setPassword(event.target.value)}
			        required
			        />
		      </Form.Group>

		      {
		      	isActive ?
		      	<Button variant="primary" type="submit">
		        Login
		      </Button>
		      :
		      <Button variant="secondary" type="submit" disabled>
		        Login
		      </Button>
		      
		      }

		      
		    </Form>
		 </Fragment>
		)
}