import { Link } from "react-router-dom"
import {Container, Image} from 'react-bootstrap'

export default function NotFound() {
	
	return(
		<Container>
		<div>
		{/*<Image = 'https://thumbs.dreamstime.com/b/page-not-found-error-hand-drawn-vector-layout-template-broken-robot-your-website-projects-76842565.jpg'></Image>*/}
		<h1 className = 'my-3'>Page Not Found</h1>
		<p>Go back to the <Link to = '/'>homepage</Link>.</p>
		</div>
		</Container>
	)
}