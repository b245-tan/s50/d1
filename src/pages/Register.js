import {Button, Form} from 'react-bootstrap';
import {Fragment} from 'react';

// import the hooks that are needed in our page
import {useContext, useState, useEffect} from 'react'
import {Navigate, useNavigate} from "react-router-dom"
import Swal from 'sweetalert2'

import UserContext from '../UserContext.js'


export default function Register(){
	// Create 3 new states where we will store value from the input of the email, password, and confirmPassword

	const [firstName, setfirstName] = useState("");
	const [lastName, setlastName] = useState("");
	const [mobileNo, setmobileNo] = useState("");		
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [confirmPassword, setConfirmPassword] = useState("");

	const navigate = useNavigate();

	// const [user, setUser] = useState(localStorage.getItem("email"))

	const {user, setUser} = useContext(UserContext)

	// create another state for the button
	const [isActive, setIsActive] = useState(false)

	/*useEffect(() => {
		console.log(email)
		console.log(password)
		console.log(confirmPassword)
	}, [email, password, confirmPassword])*/

	useEffect(() => {
		if(email !== "" && password !== "" && confirmPassword !== "" && password === confirmPassword){
			setIsActive(true)
		}
		else {
			setIsActive(false)
		}
	}, [email, password, confirmPassword])



	function register(event){
		event.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/user/register`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				password: password,
				mobileNo: mobileNo
			})
		})
		.then(result => result.json())
		.then(data => {
			console.log(data)
			if(data === true) {
				Swal.fire({
					title: "Authentication successful!",
					icon: "success",
					text: "Welcome to Zuitt!",
					showConfirmButton: false,
					timer: 2000
				})

				navigate("/login")
			}
			else{

				Swal.fire({
					title: "Registration failed!",
					icon: "error",
					text: "Check your credentials!",
					showConfirmButton: false,
					timer: 1500
				})
			}
		})

	

		/*localStorage.setItem("email", email)
		setUser(localStorage.getItem("email"))

		alert("Congratulations, you are now registered!")

		setEmail('')
		setPassword('')
		setConfirmPassword('')

		navigate("/")*/
	}



	return(

		user ?
		<Navigate to = "/*"/>
		:
		<Fragment>
		<h1 className = "text-center mt-5">Register</h1>
		<Form className = 'mt-5' onSubmit = {event=> register(event)}>
				<Form.Group className="mb-3" controlId="formBasicFirstName">
		        <Form.Label>First Name</Form.Label>
		        <Form.Control 
			        type="string" 
			        placeholder="Enter your first name" 
			        value ={firstName}
			        onChange = {event => setfirstName(event.target.value)}
			        required
			        />
		      </Form.Group>

		      <Form.Group className="mb-3" controlId="formBasicLastName">
		        <Form.Label>Last Name</Form.Label>
		        <Form.Control 
			        type="string" 
			        placeholder="Enter your last name" 
			        value ={lastName}
			        onChange = {event => setlastName(event.target.value)}
			        required
			        />
		      </Form.Group>

		      <Form.Group className="mb-3" controlId="formBasicEmail">
		        <Form.Label>Email Address</Form.Label>
		        <Form.Control 
			        type="email" 
			        placeholder="Enter email" 
			        value ={email}
			        onChange = {event => setEmail(event.target.value)}
			        required
			        />
		        <Form.Text className="text-muted">
		          We'll never share your email with anyone else.
		        </Form.Text>
		      </Form.Group>

		      <Form.Group className="mb-3" controlId="formBasicMobileNo">
		        <Form.Label>Mobile Number</Form.Label>
		        <Form.Control 
			        type="string" 
			        placeholder="Enter your mobile number" 
			        value ={mobileNo}
			        onChange = {event => setmobileNo(event.target.value)}
			        required
			        />
		      </Form.Group>

		      <Form.Group className="mb-3" controlId="formBasicPassword">
		        <Form.Label>Password</Form.Label>
		        <Form.Control 
			        type="password" 
			        placeholder="Password" 
			        value ={password}
			        onChange = {event => setPassword(event.target.value)}
			        required
			        />
		      </Form.Group>

		      <Form.Group className="mb-3" controlId="formBasicConfirmPassword">
		        <Form.Label>Confirm Password</Form.Label>
		        <Form.Control 
			        type="password" 
			        placeholder="Confirm your password" 
			        value ={confirmPassword}
			        onChange = {event => setConfirmPassword(event.target.value)}
			        required
			        />
		      </Form.Group>

		      {
		      	isActive ?
		      	<Button variant="primary" type="submit">
		        Submit
		      </Button>
		      :
		      <Button variant="danger" type="submit" disabled>
		        Submit
		      </Button>
		      
		      }

		      
		    </Form>
		 </Fragment>
		)
}